# change-branch-deletes-filled-fields-in-mr

Projects for screenshots and examples of bugreport


Steps to reproduce:
------------------
1. Create a repository (e.g git@gitlab.com:oplatek/change-branch-deletes-filled-fields-in-mr.git) with a default branch `master`
2. Create a new branch `devel` by `git checkout -b devel`
3. Push the new branch to gitlab `git push -u origin devel`
4. Create a new branch based on `devel` e.g. `git checkout -b update-readme-on-devel-branch` and commit changes to readme
5. Push the branch to the origin (gitlab.com GitLab Enterprise Edition 11.1.2-ee f5babb0) `git push -u origin update-readme-on-devel-branch`
6. Create the Merge Request (MR) e.g. by clicking the [link](https://gitlab.com/oplatek/change-branch-deletes-filled-fields-in-mr/merge_requests/new?merge_request%5Bsource_branch%5D=update-readme-on-devel-branch) as displayed after pushing the branch to gitlab.com origin.
7. **Fill the description, assignee, milestone, labels, check remove source branch etc.** all the details in the MR.
8. Finally you realized that you **do not want to merge the branch into master but into devel. You change the branches and all the filled fields disappear**.

Bug description
---------------
See the steps 7. and 8. in Steps to Reproduce section.

Suggested Improvement
---------------------
In the new MR to `devel` branch instead of `master` branch keep the original fields values from the previous MR to `master` branch.

Suggested Workaround
--------------------
If suggested improvement is to complicated, it would help to move this checkbox at the top because users are typically filling the form from the top to the bottom.
Now they hit `change branch` box at the bottom with the whole form filled and **all the work is thrown away**.
